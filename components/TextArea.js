
/**
 * Remplacez ce composant par un composant comprenant un TextInput Multiline accompagné d'un compteur de mots:
 *  - Le composant doit être composé
 *      - d'un TextInput permettant la saisie
 *      - d'un Text englobant deux autres éléments Text : l'un contenant le nombre de mots, 
 *        le second contenant la chaîne " mots" (avec l'espace). (ex : "8 mots" avec 8 en gras)
 *  - Le TextInput doit avoir le prop `multiline` à `true` et prendre toute la place disponible en largeur
 *  - Le TextInput doit prendre le plus de taille possible en hauteur sans écraser le texte en dessous ou dépasser la taille de son conteneur
 *  - Il doit avoir une propriété de style `textAlignVertical` à `top`
 *  - L'élément Text comprenant le nombre de mots doit être affiché en gras.
 *  - Lors de la saisie de texte dans l'input, le nombre de mots (séparés par des espaces) doit être mis à jour dynamiquement
 *  - On utilisera la méthode `split` de la classe String Javascript pour compter le nombre de mots en ne considérant que les espaces.
 *  - Le composant aura un prop "updateText" de type callback qui se déclenche dynamiquement lorsque le texte de l'input est modifié,
 *    avec comme argument la nouvelle valeur de l'input (utilisez le prop `onChangeText`)
 */


export default () => {
    return (
        <></>
    );
    
};
