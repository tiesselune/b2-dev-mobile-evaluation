import { StyleSheet } from "react-native";

/**
 * Remplacez ce composant de manière à ce qu'il affiche un prop `text` formatté de la manière suivante :
 *  - Les parties de texte entourées par des étoiles * seront affichées en gras
 *  - Les parties de texte entourées par des underscores _ seront affichées en italique.
 * 
 * La fonction permettant d'analyser le texte (analyzeText) vous est fournie ci-dessous. Elle renvoie un tableau d'objets ayant
 * les propriétés suivantes :
 *  - text : Un fragment du texte
 *  - style : "normal" ou "italic"
 *  - weight : "normal" ou "bold"
 * 🚧 Un fragment peut être à la fois gras et italique!
 * 
 * Votre composant comportera :
 * - Un composant Text
 * - En enfant de ce Text, autant de composants Text que de fragments obtenus avec la fonction `analyzeText`
 *      - 💡 chaque fragment devra être affiché en gras (`fontWeight` à `bold`) et/ou en italique (`fontStyle` à `italic`)
 *        selon les propriétés du fragment associé
 */

export default () => {
    return (
        <></>
    );
};


const styles = StyleSheet.create({
    container : {
        flex : 1,
    }
})


const analyzeText = (text) => {
    if(!text){
        return [];
    }
    return _analyzeTextRec([],"normal","normal",text);
};


const _analyzeTextRec = (currentFragments, currentWeight, currentStyle, remainingText) => {
    let fragment = "";
    for(let i = 0; i < remainingText.length; i++){
        const letter = remainingText[i];
        if(letter === "*"){
            if(fragment.length > 0){
                currentFragments.push({weight : currentWeight, style : currentStyle, text : fragment});
            }
            return _analyzeTextRec(
                currentFragments,
                currentWeight == "bold" ? "normal" : "bold", 
                currentStyle,
                remainingText.substring(i+1)
            );
        }
        if(letter === "_"){
            if(fragment.length > 0){
                currentFragments.push({weight : currentWeight, style : currentStyle, text : fragment});
            }
            return _analyzeTextRec(
                currentFragments,
                currentWeight,
                currentStyle  == "italic" ? "normal" : "italic",
                remainingText.substring(i+1)
            );
        }
        fragment += letter;
    }
    if(fragment.length > 0){
        currentFragments.push({weight : currentWeight, style : currentStyle, text : fragment});
    }
    return currentFragments;
}