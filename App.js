import { StyleSheet, Text, View, KeyboardAvoidingView, SafeAreaView } from 'react-native';
import TextArea from "./components/TextArea.js";
import TextPreview from "./components/TextPreview.js";
import {useState} from "react";

/*
 * TODO : Commencez par aller compléter les composants TextArea et TextPreview indépendamment.
 * Ensuite, ajoutez ce qu'il faut à ces trois composants si nécessaire pour qu'éditer le texte dans TextArea mette à jour 
 * automatiquement le texte dans TextPreview, dynamiquement.
 */

export default function App() {
  const [text,setText] = useState("");
  return (
    <SafeAreaView style={styles.container}>
    <KeyboardAvoidingView style={{flex : 1}}>
      <View style={styles.half}>
      <Text style={styles.title}>Type your text :</Text>
        <TextArea></TextArea>
      </View>
      <View style={{...styles.half, ...styles.preview}}>
        <Text style={styles.title}>Preview</Text>
        <TextPreview></TextPreview>
      </View>
    </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    margin : 10,
    marginTop : 40
  },
  half : {
    flex : 1,
    borderRadius : 5,
    backgroundColor : "#99c1f1",
    margin : 10,
    padding : 10,
  },
  preview : {
    backgroundColor : "#c4dbf7",
  },
  title :  {
    fontWeight : "bold",
    fontSize : 18,
    color : "white",
    borderBottomColor : "white",
    borderBottomWidth : 2,
    borderStyle : "solid",
    padding : 3,
    marginBottom : 5,
  }
});
