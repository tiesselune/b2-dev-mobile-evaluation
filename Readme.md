# B2 Informatique - Evaluation

---

## Principe de l'évaluation

Il s'agira de compléter le code de ce dépôt de manière à créer un petit éditeur de texte formatté en temps réel.

L'écran est divisé en deux parties : 
 - En haut, un éditeur dans lequel vous pourrez écrire du texte et qui en comptera dynamiquement les mots au fur et à mesure que vous tapez, en affichant le nombre de mots en bas à gauche.
 - En bas, un aperçu qui mettra en forme le texte tappé plus haut : 
    - Les parties entourées d'étoiles `*` seront **en gras**
    - Les parties entournées d'underscores `_` seront *en italique*.

💡 Le code permettant d'analyser le texte vous est fourni.

Le but de cette évaluation est de valider la liste des tests automatiques dont la liste et le mode de fonctionnement vous est donné plus bas.

---

## Etape 0 : Cloner et installer le projet

Clônez ce dépôt, puis placez-vous dans son dossier et tappez :

```bash
npm install
```

💡 Assurez-vous d'avoir installé `expo-cli`!

Une fois l'installation terminée, lancez le projet avec 

```bash
npm run start
```

ou 

```bash
expo start
```

---

## Etape 1 : Complétez `TextArea.js`

 - Complétez le composant `TextArea.js`. Les contraintes sont listées sous forme de commentaires dans le fichier lui-même.

 - Testez son fonctionnement avec
```bash
npm run test:TextArea
```

### Liste des tests

 1. **`has a root View element`** Votre élément possède un élément `View` à sa racine
 1. **`has two children, one TextInput and one Text`** Cette View possède deux enfants directs, un `TextInput` et un `Text`
 1. **`has a Text child that has 2 text children of its own`** Le Text cité plus haut possède deux enfants `Text` directs
 1. **`second text child should contain " mots" as a string`** Le second des deux `Text` cités ci-dessus contient la chaîne "` mots`" (avec l'espace)
 1. **`TextInput has the multiline prop set to true`** Le TextInput possède le prop `multiline` à `true`
 1. **`TextInput should have text aligned to top`** Le texte du `TextInput` est aligné en haut verticalement avec `textAlignVertical` à `top`
 1. **`Text containing the number of words should be in bold`** Le `Text` le nombre de mots (le premier enfant de son parent) devrait être en gras avec `fontStyle` à `bold`
 1. **`Updating text triggers the "updateText" callback prop`** Le composant `TextArea` accepte un prop `updateText` en callback qui est appelé lorsqu'on modifie le contenu du `TextInput`
 1. **`Updating text triggers the "updateText" callback prop with new TextInput Value`** Ce callback est bien appelé en passant en argument la nouvelle valeur du texte dans les `TextInput`
 1. **`Updating text in TextInput should update number of words dynamically`** Le nombre de mots dans le `Text` en gras doit être modifié dynamiquement lorsqu'on change le texte de `TextInput` et correspondre au nombre de mots séparés par des espaces.

---

## Etape 2 : Complétez `TextPreview.js`

 - Complétez le composant `TextPreview.js`. Les contraintes sont listées sous forme de commentaires dans le fichier lui-même. Le code permettant de fractionner le texte vous est fourni, il vous renvoie un tableau d'objets du type `{text : String, style : String, weight : String}`

 - Testez son fonctionnement avec 
```bash
npm run test:TextPreview
```

### Liste des tests

1. **`has a Text root element`** L'élément racine de ce composant est un `Text`
1. **`has only Text grand-chilren`** Tous les enfants s'ils existent sont des éléments de type `Text`
1. **`has only one child if no special characters are present`** Si le texte fourni n'a ni `*` ni `_`, l'élément `Text` racine n'a qu'un enfant, et il s'agit aussi d'un élément `Text`
1. **`has as many children as fragments`** Si des `*` et/ou `_` sont présents, le `Text` racine a autant d'enfants que de framgments de texte séparés par `*` et `_` (ex : `Un chat *obèse* sur une branche _qui penche_` définit 4 fragments : `Un chat `,`obèse`,` sur une branche `,`qui penche`)
1. **`has appropriate style properties on bold and italic fragments`** Les fragments du texte ont les propriétés de style correctes dépendant de si ils sont entourés de `*` ou de `_` ou de rien.
1. **`can have italic and bold fragments rendered properly`** Les fragments à la fois gras et italiques sont représentés correctement

---

## Etape 3 : Liez les deux composants

 - En retouchant le code de l'application, y compris du fichier `App.js`, liez les deux composants de manière à ce que le texte écrit dans `TextArea` s'affiche, formatté, dans `TextPreview`
 - Testez le comportement de l'application avec
```bash
npm run test:App
```

### Liste des tests

1. **`should update TextPreview with content from TextArea`** Mettre à jour le texte dans le `TextArea` met à jour dynamiquement la prévisualisation dans le `TextPreview`.

---

## Lancer tous les tests

Vous pouvez lancer tous les tests avec 

```bash
npm run test
```

Votre note dépendra du nombre de tests que vous arrivez à valider!

---

# Bon courage! 💪


