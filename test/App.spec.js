import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import App from '../App.js';

describe("<App /> logic", () => {
    it("should update TextPreview with content from TextArea", () => {
        let component = render(<App />);
        let input = component.UNSAFE_getByType("TextInput");
        fireEvent.changeText(input,"Hello, this is a text");
        expect(component.getByText("Hello, this is a text")).not.toBeNull();
        expect(component.getByText("Hello, this is a text").type.displayName).toBe("Text");
        fireEvent.changeText(input,"Une poule sur un mur");
        expect(component.getByText("Une poule sur un mur")).not.toBeNull();
        expect(component.getByText("Une poule sur un mur").type.displayName).toBe("Text");
    })
});