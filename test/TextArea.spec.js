import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import TextArea from '../components/TextArea.js';

describe('<TextArea /> structure', () => {
	it("has a root View element", () => {
		const tree = render(<TextArea />).toJSON();
		expect(tree.type).toBe("View");
	})
	it('has two children, one TextInput and one Text', () => {
		const tree = render(<TextArea />).toJSON();
		expect(tree.type).toBe("View");
		expect(tree.children.length).toBe(2);
		expect(tree.children[0].type).toBe("TextInput");
		expect(tree.children[1].type).toBe("Text");
	});
	it('has a Text child that has 2 text children of its own', () => {
		const tree = render(<TextArea />).toJSON();
		expect(tree.children.length).toBe(2);
		expect(tree.children[1].children).not.toBe(null);
		expect(tree.children[1].children.length).toBe(2);
		expect(tree.children[1].children[0].type).toBe("Text");
		expect(tree.children[1].children[1].type).toBe("Text");
	});
	it('second text child should contain " mots" as a string', () => {
		const tree = render(<TextArea />).toJSON();
		expect(tree.children[1].children[1].children).not.toBe(null);
		expect(tree.children[1].children[1].children.length).toBe(1);
		expect(tree.children[1].children[1].children[0].toLowerCase()).toBe(" mots");
	})
});

describe('<TextArea /> style', () => {
	it('TextInput has the multiline prop set to true', () => {
		const tree = render(<TextArea />).toJSON();
		expect(tree.children[0].type).toBe("TextInput");
		expect(tree.children[0].props["multiline"]).toBe(true);
	});
	it('TextInput should have text aligned to top', () => {
		const tree = render(<TextArea />).toJSON();
		expect(tree.children[0].type).toBe("TextInput");
		expect(tree.children[0].props["style"].textAlignVertical).toBe("top");
	});
	it('Text containing the number of words should be in bold', () => {
		const tree = render(<TextArea />).toJSON();
		expect(tree.children[1].children[0].type).toBe("Text");
		expect(tree.children[1].children[0].props["style"].fontWeight).toBe("bold");
	})
});

describe('<TextArea /> internal logic', () => {
	it('Updating text triggers the "updateText" callback prop', async () => {
		let hasBeenCalled = false;
		const {UNSAFE_getByType,getByText} = render(<TextArea updateText={() =>{hasBeenCalled = true}}/>);
		const input = UNSAFE_getByType("TextInput");
		fireEvent.changeText(input,"New value");
		expect(hasBeenCalled).toBeTruthy();
	})
	it('Updating text triggers the "updateText" callback prop with new TextInput Value', async () => {
		let newText = "";
		const {UNSAFE_getByType,getByText} = render(<TextArea updateText={(text) =>{newText = text}}/>);
		const input = UNSAFE_getByType("TextInput");
		fireEvent.changeText(input,"New value");
		expect(newText).toBe("New value");
		fireEvent.changeText(input,"It's me, mario");
		expect(newText).toBe("It's me, mario");
	})
	it('Updating text in TextInput should update number of words dynamically', async () => {
		const {UNSAFE_getByType,getByText} = render(<TextArea updateText={() =>{}}/>);
		const input = UNSAFE_getByType("TextInput");
		fireEvent.changeText(input,"Hello, I am an automated test!");
		expect(getByText("6")).toBeTruthy();
		fireEvent.changeText(input,"Hello, This is your teacher testing your code");
		expect(getByText("8")).toBeTruthy();
		fireEvent.changeText(input,"Gimme gimme gimme a man after midnight");
		expect(getByText("7")).toBeTruthy();
	})
});
