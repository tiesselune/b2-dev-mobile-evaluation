import React from 'react';
import { render } from '@testing-library/react-native';

import TextPreview from '../components/TextPreview.js';

describe('<TextPreview /> structure', () => {
    it('has a Text root element', () => {
        const tree = render(<TextPreview />).toJSON();
        expect(tree.type).toBe("Text");
    });
    it('has only Text grand-chilren', () => {
        const tree = render(<TextPreview text="I'm blue dabadee dabada"/>).toJSON();
        if(!tree.children){
            return;
        }
        for(let child of tree.children){
            expect(child.type).toBe("Text");
        }
    });
});

describe('<TextPreview /> logic', () => {
    it('has only one child if no special characters are present', () => {
        const tree = render(<TextPreview text="Hello, world!"/>).toJSON();
        expect(tree.children.length).toBe(1);
    });
    it('has as many children as fragments', () => {
        let tree = render(<TextPreview text="Hello, *world!*"/>).toJSON();
        expect(tree.children.length).toBe(2);
        tree = render(<TextPreview text="Ceci est de l'_italique_ et ceci est du *gras*"/>).toJSON();
        expect(tree.children.length).toBe(4);
        tree = render(<TextPreview text="On peut *meme faire du gras _italique_*"/>).toJSON();
        expect(tree.children.length).toBe(3);
    });
    it('has appropriate style properties on bold and italic fragments', () => {
        let tree = render(<TextPreview text="Ceci est de l'_italique_ et ceci est du *gras*"/>).toJSON();
        expect(tree.children.length).toBe(4);
        expect(tree.children[0].props.style.fontStyle).not.toBe("italic");
        expect(tree.children[0].props.style.fontWeight).not.toBe("bold");
        expect(tree.children[1].props.style.fontWeight).not.toBe("bold");
        expect(tree.children[1].props.style.fontStyle).toBe("italic");
        expect(tree.children[2].props.style.fontStyle).not.toBe("italic");
        expect(tree.children[2].props.style.fontWeight).not.toBe("bold");
        expect(tree.children[3].props.style.fontStyle).not.toBe("italic");
        expect(tree.children[3].props.style.fontWeight).toBe("bold");
    });
    it('can have italic and bold fragments rendered properly', () => {
        let tree = render(<TextPreview text="On peut *meme faire du gras _italique_*"/>).toJSON();
        expect(tree.children.length).toBe(3);
        expect(tree.children[1].props.style.fontWeight).toBe("bold");
        expect(tree.children[2].props.style.fontWeight).toBe("bold");
        expect(tree.children[2].props.style.fontStyle).toBe("italic");
    });
});